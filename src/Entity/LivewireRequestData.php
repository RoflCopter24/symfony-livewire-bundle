<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Entity;

class LivewireRequestData
{
    public array $fingerprint;
    public array $updates;
    public array $memo;

    public function __construct(array $payload)
    {
        $this->fingerprint = $payload['fingerprint'];
        $this->updates = $payload['updates'];
        $this->memo = $payload['serverMemo'];
    }

    public static function empty($id, $name, $locale = 'en'): self
    {
        return new self([
            'fingerprint' => [ 'id' => $id, 'name' => $name, 'locale' => $locale ],
            'updates' => [],
            'serverMemo' => [],
        ]);
    }

    public function getId(): string
    {
        return $this->fingerprint['id'];
    }

    public function getName(): string
    {
        return $this->fingerprint['name'];
    }
}
