<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Middleware;

use RoflCopter24\SymfonyLivewireBundle\Component\LivewireComponent;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireRequestData;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireResponseData;

interface HydrationMiddlewareInterface
{
    public function hydrate(LivewireComponent $instance, LivewireRequestData $request): void;

    public function dehydrate(LivewireComponent $instance, LivewireResponseData $response): void;
}
