<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Middleware;

use RoflCopter24\SymfonyLivewireBundle\Component\LivewireComponent;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireRequestData;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireResponseData;
use RoflCopter24\SymfonyLivewireBundle\Event\PropertyDehydrationEvent;
use RoflCopter24\SymfonyLivewireBundle\Event\PropertyHydrationEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class CallPropertyHydrationHooks
 *
 * @package RoflCopter24\SymfonyLivewireBundle\Middleware
 */
class CallPropertyHydrationHooks implements HydrationMiddlewareInterface
{
    /**
     * @var EventDispatcherInterface
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function hydrate(LivewireComponent $instance, LivewireRequestData $request): void
    {
        $publicProperties = $instance->getPublicPropertiesDefinedBySubClass();

        foreach ($publicProperties as $property => $value) {
            $this->eventDispatcher->dispatch(new PropertyHydrationEvent($instance, $request, $property, $value), 'livewire.property.hydrate');

            // Call magic hydrateProperty methods on the component.
            // If the method doesn't exist, the __call with eat it.
            $studlyProperty = str_replace(' ', '', ucwords(str_replace(['-', '_'], ' ', $property)));

            $method = 'hydrate'.$studlyProperty;
            if (method_exists($instance, $method)) {
                $instance->{$method}($value, $request);
            }
        }
    }

    public function dehydrate(LivewireComponent $instance, LivewireResponseData $response): void
    {
        $publicProperties = $instance->getPublicPropertiesDefinedBySubClass();

        foreach ($publicProperties as $property => $value) {
            $studlyProperty = str_replace(' ', '', ucwords(str_replace(['-', '_'], ' ', $property)));

            $method = 'dehydrate'.$studlyProperty;
            if (method_exists($instance, $method)) {
                $instance->{$method}($value, $response);
            }

            $this->eventDispatcher->dispatch(new PropertyDehydrationEvent($instance, $response, $property, $value), 'livewire.property.dehydrate');
        }
    }
}
