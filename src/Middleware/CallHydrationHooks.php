<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Middleware;

use RoflCopter24\SymfonyLivewireBundle\Component\LivewireComponent;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireRequestData;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireResponseData;
use RoflCopter24\SymfonyLivewireBundle\Event\MiddlewareDehydrationEvent;
use RoflCopter24\SymfonyLivewireBundle\Event\MiddlewareHydrationEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class CallHydrationHooks
 *
 * @package RoflCopter24\SymfonyLivewireBundle\Middleware
 */
class CallHydrationHooks implements HydrationMiddlewareInterface
{
    /**
     * @var EventDispatcherInterface
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function hydrate(LivewireComponent $instance, LivewireRequestData $request): void
    {
        $this->eventDispatcher->dispatch(new MiddlewareHydrationEvent($instance, $request), 'livewire.component.hydrate');
        $this->eventDispatcher->dispatch(new MiddlewareHydrationEvent($instance, $request), 'livewire.component.hydrate.subsequent');

        $instance->hydrate($request);
    }

    public function dehydrate(LivewireComponent $instance, LivewireResponseData $response): void
    {
        $instance->dehydrate($response);

        $this->eventDispatcher->dispatch(new MiddlewareDehydrationEvent($instance, $response), 'livewire.component.dehydrate');
        $this->eventDispatcher->dispatch(new MiddlewareDehydrationEvent($instance, $response), 'livewire.component.dehydrate.subsequent');
    }

    public function initialHydrate(LivewireComponent $instance, LivewireRequestData $request): void
    {
        $this->eventDispatcher->dispatch(new MiddlewareHydrationEvent($instance, $request), 'livewire.component.hydrate');
        $this->eventDispatcher->dispatch(new MiddlewareHydrationEvent($instance, $request), 'livewire.component.hydrate.initial');
    }

    public function initialDehydrate(LivewireComponent $instance, LivewireResponseData $response): void
    {
        $instance->dehydrate($response);

        $this->eventDispatcher->dispatch(new MiddlewareDehydrationEvent($instance, $response), 'livewire.component.dehydrate');
        $this->eventDispatcher->dispatch(new MiddlewareDehydrationEvent($instance, $response), 'livewire.component.dehydrate.initial');
    }
}
