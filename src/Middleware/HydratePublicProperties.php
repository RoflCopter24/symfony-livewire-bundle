<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Middleware;

use Carbon\Carbon;
use DateTime;
use DateTimeInterface;
use Illuminate\Support\Carbon as IlluminateCarbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Stringable;
use RoflCopter24\SymfonyLivewireBundle\Component\LivewireComponent;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireRequestData;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireResponseData;
use RoflCopter24\SymfonyLivewireBundle\Exception\PublicPropertyTypeNotAllowedException;

/**
 * Class HydratePublicProperties
 *
 * @package RoflCopter24\SymfonyLivewireBundle\Middleware
 */
class HydratePublicProperties implements HydrationMiddlewareInterface
{

    public function hydrate(LivewireComponent $instance, LivewireRequestData $request): void
    {
        $publicProperties = $request->memo['data'] ?? [];

        $dates = data_get($request, 'memo.dataMeta.dates', []);
        $collections = data_get($request, 'memo.dataMeta.collections', []);
        $models = data_get($request, 'memo.dataMeta.models', []);
        $modelCollections = data_get($request, 'memo.dataMeta.modelCollections', []);
        $stringables = data_get($request, 'memo.dataMeta.stringables', []);

        foreach ($publicProperties as $property => $value) {
            if ($type = data_get($dates, $property)) {
                $types = [
                    'native' => DateTime::class,
                    'carbon' => Carbon::class,
                    'illuminate' => IlluminateCarbon::class,
                ];

                data_set($instance, $property, new $types[$type]($value));
            } else if (in_array($property, $collections, true)) {
                data_set($instance, $property, collect($value));
            }
            // TODO: implement deserialization for doctrine
            /* else if ($serialized = data_get($models, $property)) {
                static::hydrateModel($serialized, $property, $request, $instance);
            } else if ($serialized = data_get($modelCollections, $property)) {
                static::hydrateModels($serialized, $property, $request, $instance);
            }*/ else if (in_array($property, $stringables, true)) {
                data_set($instance, $property, new Stringable($value));
            } else {
                // If the value is null, don't set it, because all values start off as null and this
                // will prevent Typed properties from wining about being set to null.
                is_null($value) || $instance->$property = $value;
            }
        }
    }

    /**
     * @param LivewireComponent    $instance
     * @param LivewireResponseData $response
     * @throws PublicPropertyTypeNotAllowedException
     */
    public function dehydrate(LivewireComponent $instance, LivewireResponseData $response): void
    {
        $publicData = $instance->getPublicPropertiesDefinedBySubClass();

        data_set($response, 'memo.data', []);
        data_set($response, 'memo.dataMeta', []);

        array_walk($publicData, static function ($value, $key) use ($instance, $response) {
            if (
                // The value is a supported type, set it in the data, if not, throw an exception for the user.
                is_bool($value) || is_array($value) || is_numeric($value) || is_string($value) || is_null($value)
            ) {
                data_set($response, 'memo.data.'.$key, $value);
            }
            // TODO: implement serialization for doctrine
            /*else if ($value instanceof QueueableEntity) {
                static::dehydrateModel($value, $key, $response, $instance);
            } else if ($value instanceof QueueableCollection) {
                static::dehydrateModels($value, $key, $response, $instance);
            }*/ else if ($value instanceof Collection) {
                $response->memo['dataMeta']['collections'][] = $key;

                data_set($response, 'memo.data.'.$key, $value->toArray());
            } else if ($value instanceof DateTime) {
                if ($value instanceof IlluminateCarbon) {
                    $response->memo['dataMeta']['dates'][$key] = 'illuminate';
                } elseif ($value instanceof Carbon) {
                    $response->memo['dataMeta']['dates'][$key] = 'carbon';
                } else {
                    $response->memo['dataMeta']['dates'][$key] = 'native';
                }

                data_set($response, 'memo.data.'.$key, $value->format(DateTimeInterface::ATOM));
            } else if ($value instanceof Stringable) {
                $response->memo['dataMeta']['stringables'][] = $key;

                data_set($response, 'memo.data.'.$key, (string)$value);
            } else {
                throw new PublicPropertyTypeNotAllowedException($instance->getName(), $key);
            }
        });
    }
}
