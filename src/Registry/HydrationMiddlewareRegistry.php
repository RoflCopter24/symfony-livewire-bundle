<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Registry;

class HydrationMiddlewareRegistry implements HydrationMiddlewareRegistryInterface
{
    private array $middlewares = [];

    /**
     * @inheritDoc
     */
    public function add(string $className): HydrationMiddlewareRegistryInterface
    {
        if ($this->has($className)) {
            return $this;
        }

        $this->middlewares[] = $className;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function remove(string $className): HydrationMiddlewareRegistryInterface
    {
        if ($this->has($className)) {
            $mwIndex = (int) array_search($className, $this->middlewares, true);
            array_splice($this->middlewares, $mwIndex, 1);
        }
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function has(string $className): bool
    {
        return in_array($className, $this->middlewares, true);
    }

    /**
     * @inheritDoc
     */
    public function all(): array
    {
        return array_values($this->middlewares);
    }
}
