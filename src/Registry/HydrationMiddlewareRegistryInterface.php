<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Registry;

interface HydrationMiddlewareRegistryInterface
{
    /**
     * Adds a new HydrationMiddleware class to the registry
     *
     * @param string $className The name of the HydrationMiddleware class to register.
     * @return $this a reference to this registry instance
     */
    public function add(string $className): self;

    /**
     * Removes a HydrationMiddleware class from the registry.
     *
     * @param string $className The name of the class to remove
     * @return $this a reference to this registry instance
     */
    public function remove(string $className): self;

    /**
     * Returns whether the specified class FQN is already registered.
     *
     * @param string $className The fully qualified class name of the class to check.
     * @return bool whether the given class is registered
     */
    public function has(string $className): bool;

    /**
     * Returns all registered middlewares in this registry.
     *
     * @return array the registered middlewares in this registry.
     */
    public function all(): array;
}
