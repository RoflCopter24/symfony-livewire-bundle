<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Compiler\PriorityTaggedServiceTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class RegisterLivewireComponentsPass implements CompilerPassInterface
{
    use PriorityTaggedServiceTrait;

    private string $tag;

    public function __construct(string $tag)
    {
        $this->tag = $tag;
    }

    public function process(ContainerBuilder $container): void
    {
        if (!$this->tag || !$container->has('livewire.component_registry')) {
            return;
        }

        $this->registerComponents($container, $this->tag);
    }

    protected function registerComponents(ContainerBuilder $container, string $tag): void
    {
        $registry = $container->findDefinition('livewire.component_registry');

        foreach ($this->findAndSortTaggedServices($tag, $container) as $reference) {
            $definition = $container->findDefinition((string) $reference);
            $tags = $definition->getTag($tag);
            $definition->clearTag($tag);
            $componentClass = $definition->getClass();
            $friendlyName = $tags[0]['type'] ?? $componentClass;

            $componentId = sprintf('%s.%s', $tag, $friendlyName);

            $childDefinition = new ChildDefinition((string) $reference);
            $childDefinition->setPublic(true);
            $childDefinition->setAutowired(true);

            $registry->addMethodCall('add', [$friendlyName, $componentId]);

            $childDefinition->setTags($definition->getTags());
            $container->setDefinition($componentId, $childDefinition);
        }
    }
}
