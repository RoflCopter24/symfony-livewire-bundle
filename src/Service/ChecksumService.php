<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Service;

use JsonException;

class ChecksumService
{
    /**
     * @var string The global app secret defined in the symfony .env file
     */
    private string $appSecret;

    public function __construct(string $appSecret)
    {
        $this->appSecret = $appSecret;
    }

    /**
     * Generate a sha256 based hmac hash for
     *  1. The fingerprint of the component (including its unique id)
     *  2. The current server memo representing the component state.
     *
     * @param array $fingerprint The fingerprint that uniquely identifies a component
     * @param array $memo        The server memo data that represents the components state.
     * @return string The generated hmac value.
     * @throws JsonException when serializing the fingerprint and memo arrays failed.
     */
    public function generate(array $fingerprint, array $memo): string
    {
        // It's actually Ok if the "children" tracking is tampered with.
        // Also, this way JavaScript can modify children as it needs to for
        // dom-diffing purposes.
        $memoSansChildren = array_diff_key($memo, array_flip([ 'children' ]));

        $stringForHashing = ''
            . json_encode($fingerprint, JSON_THROW_ON_ERROR)
            . json_encode($memoSansChildren, JSON_THROW_ON_ERROR);

        return hash_hmac('sha256', $stringForHashing, $this->appSecret);
    }

    /**
     * Checks the provided checksum against the given data.
     *
     * @param string $checksum    The checksum to validate.
     * @param array  $fingerprint The fingerprint that uniquely identifies a component (@see ChecksumUtil::generate)
     * @param array  $memo        The server memo data that represents the components state. (@see
     *                            ChecksumUtil::generate)
     * @return bool whether the provided checksum is valid for the provided data.
     * @throws JsonException when serializing the fingerprint and memo arrays failed. (@see ChecksumUtil::generate)
     */
    public function check(string $checksum, array $fingerprint, array $memo): bool
    {
        return hash_equals($this->generate($fingerprint, $memo), $checksum);
    }
}
