<?php
declare(strict_types=1);

namespace RoflCopter24\SymfonyLivewireBundle\Service;

class SettingsService
{
    private string $uploadDir;

    public function __construct(array $extensionConfig)
    {
        $this->uploadDir = $extensionConfig['uploadDirectory'];
    }

    /**
     * The upload directory for temporary files used by livewire.
     * Should not be shared with anything else.
     *
     * @return string
     */
    public function getUploadDir(): string
    {
        return $this->uploadDir;
    }
}
