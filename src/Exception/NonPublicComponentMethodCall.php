<?php

namespace RoflCopter24\SymfonyLivewireBundle\Exception;

class NonPublicComponentMethodCall extends \Exception
{
    public function __construct($method)
    {
        parent::__construct('Component method not found: ['.$method.']');
    }
}
