<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Exception;

class MissingRulesException extends \Exception
{
    public function __construct(string $componentName)
    {
        parent::__construct("Livewire: No validation rules for $componentName found!");
    }
}
