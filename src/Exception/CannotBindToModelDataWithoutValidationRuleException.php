<?php

namespace RoflCopter24\SymfonyLivewireBundle\Exception;

class CannotBindToModelDataWithoutValidationRuleException extends \Exception
{

    public function __construct($key, $component)
    {
        parent::__construct(
            "Cannot bind property [$key] without a validation rule present in the [\$rules] array on Livewire component: [{$component}]."
        );
    }
}
