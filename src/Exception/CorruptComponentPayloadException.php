<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Exception;

class CorruptComponentPayloadException extends \Exception
{
    public function __construct(string $componentName)
    {
        parent::__construct(
            "Livewire encountered corrupt data when trying to hydrate the [{$componentName}] component. \n".
            "Ensure that the [name, id, data] of the Livewire component wasn't tampered with between requests."
        );
    }
}
