<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Exception;

use Throwable;

class RootTagMissingFromViewException extends \Exception
{
    public function __construct(?string $componentId)
    {
        parent::__construct("Livewire encountered a missing root tag when trying to render a " .
            "component. \n When rendering a Twig view, make sure it contains a root HTML tag. Component Id: $componentId");
    }
}
