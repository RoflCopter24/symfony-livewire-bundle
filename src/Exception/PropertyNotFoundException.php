<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Exception;

class PropertyNotFoundException extends \Exception
{
    public function __construct($property, $component)
    {
        parent::__construct(
            "Property [\${$property}] not found on component: [{$component}]"
        );
    }
}
