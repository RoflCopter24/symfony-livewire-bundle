<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Controller;

use JsonException;
use Psr\Log\LoggerInterface;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireRequestData;
use RoflCopter24\SymfonyLivewireBundle\Exception\ComponentNotFoundException;
use RoflCopter24\SymfonyLivewireBundle\Manager\LifecycleManager;
use RoflCopter24\SymfonyLivewireBundle\Service\SettingsService;
use RoflCopter24\SymfonyLivewireBundle\Util\TemporaryFileUtil;
use Safe\Exceptions\FilesystemException;
use function Safe\realpath;
use function Safe\json_decode;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ApiController
 *
 * @package RoflCopter24\SymfonyLivewireBundle\Controller
 * @Route("/livewire", name="livewire.api.")
 */
class ApiController extends AbstractController
{
    protected ParameterBagInterface $appParameters;
    /**
     * @var SettingsService
     */
    private SettingsService $settingsService;
    private LoggerInterface $logger;

    public function __construct(ParameterBagInterface $parameterBag, SettingsService $settingsService, LoggerInterface $logger)
    {
        $this->appParameters = $parameterBag;
        $this->settingsService = $settingsService;
        $this->logger = $logger;
    }

    /**
     * @Route("/message/{name}", name="message", methods={"POST"})
     * @param string $name Name of the livewire component.
     * @param Request $request Request object for the request.
     * @param LifecycleManager $lifecycleManager The LifecycleManager used to manage the components.
     * @return JsonResponse The resulting message back to the frontend.
     * @throws ComponentNotFoundException When the component with the given name/id is not found in the container.
     * @throws \Safe\Exceptions\JsonException|JsonException When the json parsing failed
     */
    public function message(string $name, Request $request, LifecycleManager $lifecycleManager): JsonResponse
    {
        if (0 !== strpos($request->headers->get('Content-Type'), 'application/json')) {
            throw new BadRequestException("thou shall provide json!");
        }
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $livewireData = new LivewireRequestData($data);

        return $this->json(
            $lifecycleManager
                ->subsequentRequest($livewireData)
                ->toSubsequentResponse()
        );
    }

    /**
     * @Route("/upload-file", name="file_upload", methods={"POST"})
     * @param Request $request Request object for the request
     * @return JsonResponse Response containing the uploaded file paths
     */
    public function upload(Request $request): JsonResponse
    {
        $files = $request->files->get('files');

        $storagePath = $this->appParameters->get('kernel.project_dir').$this->settingsService->getUploadDir();
        try {
            // try to use realpath to resolve any symlinks we come across.
            $resolvedPath = realpath($storagePath);
            $storagePath = $resolvedPath;
        } catch (FilesystemException $e) {
            $this->logger->error('[LivewireApiController] upload: Resolving of path failed with: '. $e->getMessage(), $e->getTrace());
        }

        $fileHashParts = collect($files)
            ->map(function (UploadedFile $file) use ($storagePath) {
                $filename = TemporaryFileUtil::generateHashNameWithOriginalNameEmbedded($file);
                return $file->move($storagePath, $filename)->getRealPath();
            })
            ->map(function (string $path) use ($storagePath) {
                // remove the upload directory path and keep only the "/filename.ext" part.
                return str_replace($storagePath, '', $path);
            });

        return $this->json(['paths' => $fileHashParts]);
    }

}
