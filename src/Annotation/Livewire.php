<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Annotation;

use Doctrine\Common\Annotations\Annotation\Attribute;
use Doctrine\Common\Annotations\Annotation\Attributes;
use Doctrine\Common\Annotations\Annotation\Target;
use Terminal42\ServiceAnnotationBundle\Annotation\ServiceTagInterface;

/**
 * @Annotation
 * @Target("CLASS")
 * @Attributes({
 *   @Attribute("value", type="string")
 * })
 */
class Livewire implements ServiceTagInterface
{
    private array $attributes;

    public function __construct(array $attributes)
    {
        if (isset($attributes['value'])) {
            $attributes['type'] = $attributes['value'];
            unset($attributes['value']);
        }

        $this->attributes = $attributes;
    }

    public function getName(): string
    {
        return 'livewire.component';
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }
}
