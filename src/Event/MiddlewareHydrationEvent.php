<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Event;

use RoflCopter24\SymfonyLivewireBundle\Component\LivewireComponent;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireRequestData;

class MiddlewareHydrationEvent extends MiddlewareEvent
{
    /**
     * @var LivewireRequestData
     */
    protected LivewireRequestData $requestData;

    public function __construct(LivewireComponent $component, LivewireRequestData $requestData)
    {
        parent::__construct($component);
        $this->requestData = $requestData;
    }

    /**
     * @return LivewireRequestData
     */
    public function getRequestData(): LivewireRequestData
    {
        return $this->requestData;
    }
}
