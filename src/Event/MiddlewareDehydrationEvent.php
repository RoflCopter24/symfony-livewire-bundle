<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Event;

use RoflCopter24\SymfonyLivewireBundle\Component\LivewireComponent;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireResponseData;

class MiddlewareDehydrationEvent extends MiddlewareEvent
{
    /**
     * @var LivewireResponseData
     */
    protected LivewireResponseData $responseData;

    public function __construct(LivewireComponent $component, LivewireResponseData $responseData)
    {
        parent::__construct($component);
        $this->responseData = $responseData;
    }

    /**
     * @return LivewireResponseData
     */
    public function getResponseData(): LivewireResponseData
    {
        return $this->responseData;
    }

}
