<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\EventListener;

use RoflCopter24\SymfonyLivewireBundle\Event\MiddlewareDehydrationEvent;
use RoflCopter24\SymfonyLivewireBundle\Event\MiddlewareHydrationEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SupportChildren implements EventSubscriberInterface
{
    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'livewire.component.dehydrate' => ['onDehydration', 0],
            'livewire.component.hydrate.subsequent' => ['onSubsequentHydration', 0]
        ];
    }

    public static function onDehydration(MiddlewareDehydrationEvent $event): void
    {
        $event->getResponseData()->memo['children'] = $event->getComponent()->getRenderedChildren();
    }

    public static function onSubsequentHydration(MiddlewareHydrationEvent $event): void
    {
        $event->getComponent()->setPreviouslyRenderedChildren($event->getRequestData()->memo['children']);
    }
}
