<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Manager;

use Psr\Log\LoggerInterface;
use RoflCopter24\SymfonyLivewireBundle\Component\LivewireComponent;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireRequestData;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireResponseData;
use RoflCopter24\SymfonyLivewireBundle\Exception\ComponentNotFoundException;
use RoflCopter24\SymfonyLivewireBundle\Registry\HydrationMiddlewareRegistryInterface;
use RoflCopter24\SymfonyLivewireBundle\Registry\LivewireComponentRegistryInterface;
use RoflCopter24\SymfonyLivewireBundle\Util\UuidUtil;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class LifecycleManager
{
    private LivewireComponentRegistryInterface $componentRegistry;

    private HydrationMiddlewareRegistryInterface $hydrationRegistry;

    private HydrationMiddlewareRegistryInterface $initialHydrationRegistry;

    private HydrationMiddlewareRegistryInterface $initialDehydrationRegistry;

    private ContainerInterface $container;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;
    private string $locale;
    /**
     * @var SessionInterface
     */
    private SessionInterface $session;

    public function __construct(LivewireComponentRegistryInterface $registry,
                                HydrationMiddlewareRegistryInterface $hydrationMiddlewareRegistry,
                                HydrationMiddlewareRegistryInterface $initialHydrationMiddlewareRegistry,
                                HydrationMiddlewareRegistryInterface $initialDehydrationMiddlewareRegistry,
                                ContainerInterface $container,
                                LoggerInterface $logger,
                                RequestStack $requestStack,
                                SessionInterface $session)
    {
        $this->componentRegistry = $registry;
        $this->hydrationRegistry = $hydrationMiddlewareRegistry;
        $this->initialHydrationRegistry = $initialHydrationMiddlewareRegistry;
        $this->initialDehydrationRegistry = $initialDehydrationMiddlewareRegistry;
        $this->container = $container;
        $this->logger = $logger;
        $this->locale = $requestStack->getMasterRequest()->getLocale();
        $this->session = $session;
    }

    /**
     * Returns the generated html code for the initial render of the component.
     *
     * @param string $componentName The friendly name of the component
     * @param mixed ...$params
     * @return string The generated HTML code.
     * @throws ComponentNotFoundException
     */
    public function initialRequest(string $componentName, ...$params): string
    {
        $this->logger->debug('[Livewire] Initial request for component ' . $componentName);

        $componentServiceId = $this->componentRegistry->get($componentName);
        $this->logger->debug("[Livewire] ServiceId for component $componentName is $componentServiceId");

        if (!$componentServiceId) {
            $this->logger->critical("[Livewire] Component with name $componentName does not exist!");
            throw new ComponentNotFoundException($componentName);
        }

        $component = $this->container->get($componentServiceId);
        if (!$component) {
            $this->logger->critical("[Livewire] Component with serviceId $componentServiceId does not exist!");
            throw new ComponentNotFoundException($componentServiceId);
        }

        $component->setId(UuidUtil::generate());
        $component->setName($componentName);

        $this->session->set('_locale', $this->locale);

        $initialRequestData = LivewireRequestData::empty($component->getId(), $componentName, $this->locale ?? 'en');

        $this->logger->debug("[Livewire] Initial hydration for $componentName (ServiceId $componentServiceId).");
        $this->initialHydrate($component, $initialRequestData);

        $this->logger->debug("[Livewire] Mount called for $componentName (ServiceId $componentServiceId).");
        $component->mount(...$params);

        $this->logger->debug("[Livewire] Initial render for $componentName (ServiceId $componentServiceId).");
        $component->renderToView();
        $initialResponseData = LivewireResponseData::fromRequest($initialRequestData);

        $this->logger->debug("[Livewire] Initial dehydration for $componentName (ServiceId $componentServiceId).");
        $this->initialDehydrate($component, $initialResponseData);

        $initialResponseData->embedThyselfInHtml();

        $this->logger->debug("[Livewire] Finishing initial request for $componentName (ServiceId $componentServiceId).");
        return $initialResponseData->toInitialResponse();
    }

    /**
     * Returns the generated HTML code and request data for subsequent (AJAX) requests.
     *
     * @param LivewireRequestData $requestData The livewire request data gathered from the incoming request.
     * @return LivewireResponseData The outgoing response data containing the generated HTML and updated request data.
     * @throws ComponentNotFoundException
     */
    public function subsequentRequest(LivewireRequestData $requestData): LivewireResponseData
    {
        $componentName = $requestData->getName();
        $this->logger->debug('[Livewire] Initial request for component ' . $componentName);

        $serviceId = $this->componentRegistry->get($componentName);
        $this->logger->debug("[Livewire] ServiceId for component $componentName is $serviceId");

        if (!$serviceId) {
            $this->logger->critical("[Livewire] Component with name $componentName does not exist!");
            throw new ComponentNotFoundException($serviceId);
        }

        $componentInstance = $this->container->get($serviceId);

        if (!$componentInstance) {
            $this->logger->critical("[Livewire] Component with serviceId $serviceId does not exist!");
            throw new ComponentNotFoundException($serviceId);
        }

        $componentInstance->setName($componentName);
        $componentInstance->setId($requestData->getId());
        $this->logger->debug("[Livewire] Subsequent hydration for $componentName (ServiceId $serviceId).");
        $this->hydrate($componentInstance, $requestData);

        $this->logger->debug("[Livewire] Subsequent render for $componentName (ServiceId $serviceId).");
        $componentInstance->renderToView();
        $responseData = LivewireResponseData::fromRequest($requestData);

        $this->logger->debug("[Livewire] Subsequent dehydration for $componentName (ServiceId $serviceId).");
        $this->dehydrate($componentInstance, $responseData);

        $this->logger->debug("[Livewire] Finishing subsequent request for $componentName (ServiceId $serviceId).");

        return $responseData;
    }

    protected function initialDehydrate(LivewireComponent $instance, LivewireResponseData $responseData): void
    {
        // The array is being reversed here, so the middleware dehydrate phase order of execution is
        // the inverse of hydrate. This makes the middlewares behave like layers in a shell.
        foreach (array_reverse($this->initialDehydrationRegistry->all()) as $middlewareClass) {
            $middleware = $this->container->get($middlewareClass);
            if (!$middleware) {
                continue;
            }
            method_exists($middleware, 'initialDehydrate')
                ? $middleware->initialDehydrate($instance, $responseData)
                : $middleware->dehydrate($instance, $responseData);
        }
    }

    protected function initialHydrate(LivewireComponent $instance, LivewireRequestData $data): void
    {
        foreach ($this->initialHydrationRegistry->all() as $middlewareClass) {
            $middleware = $this->container->get($middlewareClass);
            if (!$middleware) {
                continue;
            }
            method_exists($middleware, 'initialHydrate')
                ? $middleware->initialHydrate($instance, $data)
                : $middleware->hydrate($instance, $data);
        }
    }

    protected function dehydrate(LivewireComponent $instance, LivewireResponseData $responseData): void
    {
        // The array is being reversed here, so the middleware dehydrate phase order of execution is
        // the inverse of hydrate. This makes the middlewares behave like layers in a shell.
        foreach (array_reverse($this->hydrationRegistry->all()) as $middlewareClass) {
            $middleware = $this->container->get($middlewareClass);
            if (!$middleware) {
                continue;
            }
            $middleware->dehydrate($instance, $responseData);
        }
    }

    protected function hydrate(LivewireComponent $instance, LivewireRequestData $data): void
    {
        foreach ($this->hydrationRegistry->all() as $middlewareClass) {
            $middleware = $this->container->get($middlewareClass);
            if (!$middleware) {
                continue;
            }
            $middleware->hydrate($instance, $data);
        }
    }

}
