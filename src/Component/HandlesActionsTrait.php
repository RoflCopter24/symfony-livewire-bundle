<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Component;

use Illuminate\Support\Str;
use ReflectionClass;
use ReflectionMethod;
use RoflCopter24\SymfonyLivewireBundle\Event\ActionReturnedEvent;
use RoflCopter24\SymfonyLivewireBundle\Exception\MethodNotFoundException;
use RoflCopter24\SymfonyLivewireBundle\Exception\MissingFileUploadsTraitException;
use RoflCopter24\SymfonyLivewireBundle\Exception\NonPublicComponentMethodCall;
use RoflCopter24\SymfonyLivewireBundle\Exception\PublicPropertyNotFoundException;
use RoflCopter24\SymfonyLivewireBundle\Middleware\HashDataPropertiesForDirtyDetection;

trait HandlesActionsTrait
{
    use InteractsWithPropertiesTrait;

    public function syncInput($name, $value, $rehash = true): void
    {
        $propertyName = $this->beforeFirstDot($name);

        /*if (($this->{$propertyName} instanceof Entity || $this->{$propertyName} instanceof PersistentCollection)
            && $this->missingRuleFor($name)) {
            new CannotBindToModelDataWithoutValidationRuleException($name, $this->getName());
        }*/

        $this->callBeforeAndAfterSyncHooks($name, $value, function ($name, $value) use ($propertyName, $rehash) {
            if ($this->propertyIsPublicAndNotDefinedOnBaseClass($propertyName)) {
                new PublicPropertyNotFoundException($propertyName, $this->getName());
            }

            if ($this->containsDots($name)) {
                // Strip away model name.
                $keyName = $this->afterFirstDot($name);
                // Get model attribute to be filled.
                $targetKey = $this->beforeFirstDot($keyName);

                // Get existing data from model property.
                $results = [];
                $results[$targetKey] = data_get($this->{$propertyName}, $targetKey, []);

                // Merge in new data.
                data_set($results, $keyName, $value);

                // Re-assign data to model.
                data_set($this->{$propertyName}, $targetKey, $results[$targetKey]);
            } else {
                $this->{$name} = $value;
            }

            $rehash
            && $this->services
                ->get(HashDataPropertiesForDirtyDetection::class)
                ->rehashProperty($name, $value, $this);
        });
    }

    public function callMethod(string $method, array $params = []): void
    {
        switch ($method) {
            case '$sync':
                $prop = array_shift($params);
                $this->syncInput($prop, head($params));

                return;

            case '$set':
                $prop = array_shift($params);
                $this->syncInput($prop, head($params), $rehash = false);

                return;

            case '$toggle':
                $prop = array_shift($params);
                $this->syncInput($prop, !$this->{$prop}, $rehash = false);

                return;

            case '$refresh':
                return;
        }

        if (!method_exists($this, $method)) {
            if ($method === 'startUpload') {
                new MissingFileUploadsTraitException($this);
            }

            throw new MethodNotFoundException($method, $this->getName());
        }

        if (!$this->methodIsPublicAndNotDefinedOnBaseClass($method)) {
            new NonPublicComponentMethodCall($method);
        }

        $returned = $this->$method(...$params);

        $this->services
            ->get('event_dispatcher')
            ->dispatch(new ActionReturnedEvent($this, $method, $returned), 'livewire.action.returned');
    }

    protected function callBeforeAndAfterSyncHooks($name, $value, $callback): void
    {
        $name = Str::of($name);

        $propertyName = $name->studly()->before('.');
        $keyAfterFirstDot = $name->contains('.') ? $name->after('.') : null;
        $keyAfterLastDot = $name->contains('.') ? $name->afterLast('.') : null;

        $beforeMethod = 'updating' . $propertyName;
        $afterMethod = 'updated' . $propertyName;

        $beforeNestedMethod = $name->contains('.')
            ? 'updating' . $name->replace('.', '_')->studly()
            : false;

        $afterNestedMethod = $name->contains('.')
            ? 'updated' . $name->replace('.', '_')->studly()
            : false;

        $name = $name->__toString();
        $this->updating($name, $value);

        if (method_exists($this, $beforeMethod)) {
            $this->{$beforeMethod}($value, $keyAfterFirstDot);
        }

        if ($beforeNestedMethod && method_exists($this, $beforeNestedMethod)) {
            $this->{$beforeNestedMethod}($value, $keyAfterLastDot);
        }

        $this->services
            ->get('event_dispatcher')
            ->dispatch($this, 'livewire.component.updating');

        $callback($name, $value);

        $this->updated($name, $value);

        if (method_exists($this, $afterMethod)) {
            $this->{$afterMethod}($value, $keyAfterFirstDot);
        }

        if ($afterNestedMethod && method_exists($this, $afterNestedMethod)) {
            $this->{$afterNestedMethod}($value, $keyAfterLastDot);
        }

        $this->services
            ->get('event_dispatcher')
            ->dispatch($this, 'livewire.component.updated');
    }

    protected function methodIsPublicAndNotDefinedOnBaseClass($methodName): bool
    {
        return collect((new ReflectionClass($this))->getMethods(ReflectionMethod::IS_PUBLIC))
                ->reject(function ($method) {
                    // The "render" method is a special case. This method might be called by event listeners or other ways.
                    if ($method === 'render') {
                        return false;
                    }

                    return $method->class === self::class;
                })
                ->pluck('name')
                ->search($methodName) !== false;
    }
}
