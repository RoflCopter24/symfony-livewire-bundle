<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Component;

use BadMethodCallException;
use Illuminate\Support\Str;
use Illuminate\Support\Traits\Macroable;
use LogicException;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireRequestData;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireResponseData;
use RoflCopter24\SymfonyLivewireBundle\Exception\PropertyNotFoundException;
use RoflCopter24\SymfonyLivewireBundle\Service\ComponentServicesInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\TemplateWrapper;

abstract class LivewireComponent
{
    use Macroable {
        __call as macroCall;
    }
    use InteractsWithPropertiesTrait;
    use HandlesActionsTrait;
    use ValidatesInputTrait;
    use TracksRenderedChildrenTrait;
    use ReceivesEvents;

    public bool $shouldSkipRender = false;

    protected array $computedPropertyCache = [];

    protected TemplateWrapper $preRenderedView;

    protected array $renderContext = [];
    /**
     * @var ComponentServicesInterface
     */
    protected ComponentServicesInterface $services;

    private ?string $id;

    private ?string $name;

    public string $redirectTo;

    public int $redirectStatus;


    public function __construct(ComponentServicesInterface $componentServices)
    {
        $this->services = $componentServices;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $uuid): void
    {
        $this->id = $uuid;
    }

    public function getName(): string
    {
        return $this->name ?? static::class;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function renderToView(): TemplateWrapper
    {
        $this->services->eventDispatcher()->dispatch($this, 'livewire.component.rendering');

        $view = method_exists($this, 'render')
            ? $this->{'render'}()
            : $this->view("livewire/{$this->getName()}.html.twig", array_merge($this->renderContext, $this->getPublicPropertiesDefinedBySubClass()));

        $this->services->eventDispatcher()->dispatch($this, 'livewire.component.rendered');

        return $this->preRenderedView = $view;
    }

    public function output($errors = null): ?string
    {
        if ($this->shouldSkipRender) {
            return null;
        }

        $view = $this->preRenderedView;
        $properties = $this->getPublicPropertiesDefinedBySubClass();

        $output = $view->render(array_merge($this->renderContext, $properties));

        $this->services->eventDispatcher()->dispatch($view, 'livewire.view:render');

        return $output;
    }

    public function __get($property)
    {
        $studlyProperty = str_replace(' ', '', ucwords(str_replace([ '-', '_' ], ' ', $property)));

        if (method_exists($this, $computedMethodName = 'get' . $studlyProperty)) {
            return $this->computedPropertyCache[$property] ?? ($this->computedPropertyCache[$property] = $this->{$computedMethodName}());
        }

        throw new PropertyNotFoundException($property, $this->getName());
    }

    public function __set($property, $value)
    {
        $studlyProperty = str_replace(' ', '', ucwords(str_replace([ '-', '_' ], ' ', $property)));

        if (method_exists($this, $computedMethodName = 'set' . $studlyProperty)) {
            return $this->computedPropertyCache[$property] ?? ($this->computedPropertyCache[$property] = $this->{$computedMethodName}($value));
        }

        throw new PropertyNotFoundException($property, $this->getName());
    }

    public function __call($method, $parameters)
    {
        if (
            in_array($method, [ 'mount', 'hydrate', 'dehydrate', 'updating', 'updated' ])
            || Str::of($method)->startsWith([ 'updating', 'updated', 'hydrate', 'dehydrate' ])
        ) {
            // Eat calls to the lifecycle hooks if the dev didn't define them.
            return null;
        }

        if (static::hasMacro($method)) {
            return $this->macroCall($method, $parameters);
        }

        throw new BadMethodCallException(sprintf(
            'Method %s::%s does not exist.', static::class, $method
        ));
    }

    protected function view(string $name, array $parameters = []): TemplateWrapper
    {
        if (!$this->services->has('twig')) {
            throw new LogicException('You can not use the "renderView" method if the Twig Bundle is not available. Try running "composer require symfony/twig-bundle".');
        }

        $this->renderContext = $parameters;
        return $this->services->get('twig')->load($name);
    }

    /**
     * Redirect the user to a url like 'https://example.com' or
     * '/admin/dashboard'.
     * If you want to use a Symfony route, use $this->redirectRoute!
     *
     * @param string $url The URL to redirect to.
     * @param int $status The optional statusCode of the redirect response. Defaults to 302.
     */
    protected function redirect(string $url, int $status = 302): void
    {
        $this->redirectTo = $url;
        $this->redirectStatus = $status;

        $this->shouldSkipRender = true;
    }

    /**
     * Redirect the user to a Symfony route with the given parameters.
     *
     * @param string $name The name of the symfony route like 'app.login'
     * @param array $parameters Optional GET parameters for the route like [uid => 2]
     * @param bool $absolute Whether the generated URL should be absolute or relative
     * @param int $status The optional statusCode of the redirect response. Defaults to 302.
     */
    protected function redirectRoute(string $name, array $parameters = [], bool $absolute = true, int $status = 302)
    {
        $this->redirectTo = $this->services
            ->urlGenerator()->generate($name, $parameters,
                $absolute ? UrlGeneratorInterface::ABSOLUTE_PATH : UrlGeneratorInterface::RELATIVE_PATH);
        $this->redirectStatus = $status;

        $this->shouldSkipRender = true;
    }

    public function hydrate(LivewireRequestData $requestData): void {}
    public function dehydrate(LivewireResponseData $responseData): void {}

    public function updating(string $name, $value): void {}
    public function updated(string $name, $value): void {}
}
