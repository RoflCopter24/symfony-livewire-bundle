<?php
/**
 * based on https://github.com/livewire/livewire/blob/v2.3.8/src/ComponentConcerns/ReceivesEvents.php
 */
declare(strict_types=1);

namespace RoflCopter24\SymfonyLivewireBundle\Component;

use RoflCopter24\SymfonyLivewireBundle\Event\LivewireEvent;

trait ReceivesEvents
{
    /**
     * @var LivewireEvent[]
     */
    protected array $eventQueue = [];
    /**
     * @var array
     */
    protected array $dispatchQueue = [];
    protected array $listeners = [];

    protected function getListeners(): array
    {
        return $this->listeners;
    }

    public function emit(string $event, ...$params): LivewireEvent
    {
        return $this->eventQueue[] = new LivewireEvent($event, $params);
    }

    public function emitUp($event, ...$params): void
    {
        $this->emit($event, ...$params)->up();
    }

    public function emitSelf($event, ...$params): void
    {
        $this->emit($event, ...$params)->self();
    }

    public function emitTo($name, $event, ...$params): void
    {
        $this->emit($event, ...$params)->component($name);
    }

    public function dispatchBrowserEvent($event, $data = null): void
    {
        $this->dispatchQueue[] = [
            'event' => $event,
            'data' => $data,
        ];
    }

    public function getEventQueue(): array
    {
        /**
         * @phpstan-ignore-next-line
         */
        return collect($this->eventQueue)->map->serialize()->toArray();
    }

    public function getDispatchQueue(): array
    {
        return $this->dispatchQueue;
    }

    protected function getEventsAndHandlers(): array
    {
        return collect($this->getListeners())
            ->mapWithKeys(function ($value, $key) {
                $key = is_numeric($key) ? $value : $key;

                return [$key => $value];
            })->toArray();
    }

    public function getEventsBeingListenedFor(): array
    {
        return array_keys($this->getEventsAndHandlers());
    }

    public function fireEvent($event, $params): void
    {
        $method = $this->getEventsAndHandlers()[$event];

        $this->callMethod($method, $params);
    }
}
