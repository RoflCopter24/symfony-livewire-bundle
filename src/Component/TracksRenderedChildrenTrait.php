<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Component;

trait TracksRenderedChildrenTrait
{
    protected array $renderedChildren = [];
    protected array $previouslyRenderedChildren = [];

    public function getRenderedChildComponentId($id)
    {
        return $this->previouslyRenderedChildren[$id]['id'];
    }

    public function getRenderedChildComponentTagName($id)
    {
        return $this->previouslyRenderedChildren[$id]['tag'];
    }

    public function logRenderedChild($id, $componentId, $tagName): void
    {
        $this->renderedChildren[$id] = [ 'id' => $componentId, 'tag' => $tagName ];
    }

    public function preserveRenderedChild($id): void
    {
        $this->renderedChildren[$id] = $this->previouslyRenderedChildren[$id];
    }

    public function childHasBeenRendered($id): bool
    {
        return array_key_exists($id, $this->previouslyRenderedChildren);
    }

    public function setPreviouslyRenderedChildren($children): void
    {
        $this->previouslyRenderedChildren = $children;
    }

    public function getRenderedChildren(): array
    {
        return $this->renderedChildren;
    }
}
