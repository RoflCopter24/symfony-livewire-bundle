<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Util;

use Ramsey\Uuid\Uuid;

class UuidUtil
{
    public static function generate(): string
    {
        return Uuid::uuid4()->toString();
    }
}
